# circleci-ruby-qt

This repository contains Docker image with:
- `ruby-2.6.5` 
- `rubygems-3.0.6`
- `bundler-2.0.2`
- `qt5`
- `build tools, like build-essential, etc...`

Based on `buildpack-deps:buster`